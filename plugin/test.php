<?php

defined('_JEXEC') or die;

class PlgContentTest extends JPlugin{
    protected $autoloadLanguage = true; 

	public function onContentAfterDisplay($context, &$article, &$params, $limitStart){
		$session = JFactory::getSession();
		$session->set("plugin_suggest_article_$article->id","visited");

		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->select('id,catid,title');
		$query->from('#__content');
		$query->where('catid="'.$article->catid.'"');

		$db->setQuery((string)$query);
		$res = $db->loadObjectList();
		
		$count=0;
		$link="";
		foreach( $res as $row ){
			if ( $count == 3 ) break;
			if ( empty($session->get("plugin_suggest_article_$row->id")) ) {
				$url = JRoute::_(ContentHelperRoute::getArticleRoute($row->id,  $row->catid));
				$link.="<a href=$url > $row->title </a> <br/>";
				$count++;
			}
		}
		return $link;
	}
}

?>